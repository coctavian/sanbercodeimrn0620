// TEST CASES BalikString

function balikString(kata) {
    let hasil = ""
    for (let i = kata.length - 1; i >= 0; i--) {
        hasil += kata[i]
    }
    return hasil
}
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"))

// TEST CASES Palindrome

function palindrome(kata1) {
    var ii = kata1.length-1;
    
    if (ii === 1 || kata1.length === 0) {
    return true;
    } 
    if (kata1[0] === kata1[ii]) {
        return palindrome(kata1.slice(1, ii));
    } 
    return false;
}
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))

// TEST CASES Bandingkan Angka

function bandingkan(num1, num2) {
    if (num1 < 0 || num2 < 0) {
        return -1
    } 
    else if (num1 === num2) {
        return -1
    } 
    else if (!num1 && !num2) {
        return -1
    } 
    else if (!num2) {
        return Number(num1)
      
    } 
    else {
        if (num1 > num2) {
            return Number(num1)
      } 
      else if (num2 > num1) {
        return Number(num2)
      }
    }
}
console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1));
console.log(bandingkan());
console.log(bandingkan("15", "18"))
  