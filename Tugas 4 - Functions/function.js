// Tugas 4 – Functions
// No. 1

function teriak() {
    return "Halo Sanbers"
}
console.log(teriak())

// No. 2
var num1 = 12;
var num2 = 4

function kalikan(){
    return num1 * num2
  }
  var hasilKali = kalikan(num1, num2)
  console.log(hasilKali) 

// No. 3

var name = "Candra Octavian";
var age = 17;
var address = "Jl. Suroyudo No.24, Tajinan, Malang";
var hobby = "Desain grafis";

function introduce(){
    return "Nama saya " + name + ", umur saya " + age + ", alamat saya " + address + ",  dan saya punya hobby yaitu " + hobby
}
var perkenalan = introduce("Nama saya " + name + ", umur saya " + age + ", alamat saya " + address + ",  dan saya punya hobby yaitu " + hobby)
console.log(perkenalan)