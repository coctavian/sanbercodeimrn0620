import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.updatePrice = this.updatePrice.bind(this)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    let hargaBaru = this.state.totalPrice += Number(price)
    this.setState({ totalPrice: hargaBaru })
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              <Text style={styles.headerText}>{this.props.route.params.userName}</Text>
            </Text>
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        <FlatList
          data={data.produk}
          renderItem={(prod) => <ListItem data={prod} updatePrice={this.updatePrice} />}
          numColumns={2}
        />
      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  render() {
    const data = this.props.data.item
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' onPress={() => this.props.updatePrice(data.harga)} />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  itemContainer: {
    width: DEVICE.width * 0.44,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10
  },
  itemImage: {
    height: 120,
    width: 120
  },
  itemName: {
    fontWeight: 'bold'
  },
  itemPrice: {
    color: 'blue'
  },
  itemStock: {
  },
  itemButton: {
  },
  buttonText: {
  }
})