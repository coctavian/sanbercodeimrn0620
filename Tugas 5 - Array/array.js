// Soal No. 1 (Range)

function range(startNum, finishNum) {
    let hasil1 = []
    if (!startNum || !finishNum) {
        return -1
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i++) {
                hasil1.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j--) {
                hasil1.push(j)
            }
        }
    }
    return hasil1
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal No. 2 (Range with Step)

function rangeWithStep(startNum, finishNum, step) {
    let hasil2 = []
    if (!startNum || !finishNum) {
        return -1
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += step) {
                hasil2.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j -= step) {
                hasil2.push(j)
            }
        }
    }
    return hasil2
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal No. 3 (Sum of Range)

function sum(startNum, finishNum, step) {
    let trueStep = 0
    let hasil3 = []

    if (!step) {
        trueStep = 1
    } else {
        trueStep = step
    }

    if (!startNum && !finishNum && !step) {
        return 0
    } else if (!finishNum) {
        return startNum
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += trueStep) {
                hasil3.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j -= trueStep) {
                hasil3.push(j)
            }
        }
    }
    return hasil3.reduce((a, b) => a + b, 0)
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

// Soal No. 4 (Array Multidimensi)

function dataHandling(input) {
    let hasil4 = '';
    for (let i = 0; i < input.length; i++) {
        console.log(`Nomor ID: ${input[i][0]}`);
        console.log(`Nama Lengkap: ${input[i][1]}`);
        console.log(`TTL: ${input[i][2]} ${input[i][3]}`);
        console.log(`Hobi: ${input[i][4]} \n`);
    }
    return hasil4
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input));

// Soal No. 5 (Balik Kata)

function balikKata(kata) {
    let hasil5 = ''
    for (let i = kata.length - 1; i >= 0; i--) {
        hasil5 += kata[i]
    }
    return hasil5
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

// Soal No. 6 (Metode Array)

function dataHandling2(input) {

    input.splice(1, 2, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung');
    input.splice(4, 1, 'Pria', 'SMA Internasional Metro');
    console.log(input)
    tanggalSplit = input[3].split('/');

    switch (tanggalSplit[1]) {
        case '01':
            console.log('Januari');
            break;
        case '02':
            console.log('Februari');
            break;
        case '03':
            console.log('Maret');
            break;
        case '04':
            console.log('April');
            break;
        case '05':
            console.log('Mei');
            break;
        case '06':
            console.log('Juni');
            break;
        case '07':
            console.log('Juli');
            break;
        case '08':
            console.log('Agustus');
            break;
        case '09':
            console.log('September');
            break;
        case '10':
            console.log('Oktober');
            break;
        case '11':
            console.log('November');
            break;
        case '12':
            console.log('Desember');
            break;
    }
    tanggalSort = tanggalSplit.sort(function (a, b) { return b - a });
    console.log(tanggalSort)
    console.log(input[3].split('/').join('-'))
    console.log(input[1].slice(0, 14))
}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)